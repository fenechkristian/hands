import detectron2
from detectron2.utils.logger import setup_logger
import logging

setup_logger()
logger = logging.getLogger(__name__)
import argparse
import numpy as np
import os, json, cv2, random, pickle
import torch
import time
import datetime
import copy

from datetime import datetime as dt

# import some common detectron2 utilities
from detectron2 import model_zoo
from detectron2.config import get_cfg
from detectron2.data import MetadataCatalog, DatasetCatalog, build_detection_train_loader
from detectron2.data import detection_utils as utils
from detectron2.evaluation import COCOEvaluator

from detectron2.structures import BoxMode
from detectron2.engine import DefaultTrainer, launch
from detectron2.engine.hooks import HookBase
from detectron2.evaluation import inference_context
from detectron2.utils.logger import log_every_n_seconds
from detectron2.data import DatasetMapper, build_detection_test_loader
import detectron2.utils.comm as comm

_ROOT_DIR = "/home/fenech/active/hcai/hand-transformer/data/freihand"
_KEYPOINT_NAMES = ['loc_wrist', 'loc_thumb_01', 'loc_thumb_02', 'loc_thumb_03',
                    'loc_thumb_04', 'loc_index_01', 'loc_index_02', 'loc_index_03',
                    'loc_index_04', 'loc_mid_01', 'loc_mid_02', 'loc_mid_03',
                    'loc_mid_04', 'loc_ring_01', 'loc_ring_02', 'loc_ring_03',
                    'loc_ring_04', 'loc_pinky_01', 'loc_pinky_02', 'loc_pinky_03',
                    'loc_pinky_04'
                    ]


class LossEvalHook(HookBase):
    def __init__(self, eval_period, model, data_loader):
        self._model = model
        self._period = eval_period
        self._data_loader = data_loader

    def _do_loss_eval(self):
        # Copying inference_on_dataset from evaluator.py
        total = len(self._data_loader)
        num_warmup = min(5, total - 1)

        start_time = time.perf_counter()
        total_compute_time = 0
        losses = []
        for idx, inputs in enumerate(self._data_loader):
            if idx == num_warmup:
                start_time = time.perf_counter()
                total_compute_time = 0
            start_compute_time = time.perf_counter()
            if torch.cuda.is_available():
                torch.cuda.synchronize()
            total_compute_time += time.perf_counter() - start_compute_time
            iters_after_start = idx + 1 - num_warmup * int(idx >= num_warmup)
            seconds_per_img = total_compute_time / iters_after_start
            if idx >= num_warmup * 2 or seconds_per_img > 5:
                total_seconds_per_img = (time.perf_counter() - start_time) / iters_after_start
                eta = datetime.timedelta(seconds=int(total_seconds_per_img * (total - idx - 1)))
                log_every_n_seconds(
                    logging.INFO,
                    "Loss on Validation  done {}/{}. {:.4f} s / img. ETA={}".format(
                        idx + 1, total, seconds_per_img, str(eta)
                    ),
                    n=5,
                )
            loss_batch = self._get_loss(inputs)
            losses.append(loss_batch)
        mean_loss = np.mean(losses)
        self.trainer.storage.put_scalar('validation_loss', mean_loss)
        comm.synchronize()

        return losses

    def _get_loss(self, data):
        # How loss is calculated on train_loop
        metrics_dict = self._model(data)
        metrics_dict = {
            k: v.detach().cpu().item() if isinstance(v, torch.Tensor) else float(v)
            for k, v in metrics_dict.items()
        }
        total_losses_reduced = sum(loss for loss in metrics_dict.values())
        return total_losses_reduced

    def after_step(self):
        next_iter = self.trainer.iter + 1
        is_final = next_iter == self.trainer.max_iter
        if is_final or (self._period > 0 and next_iter % self._period == 0):
            self._do_loss_eval()
        self.trainer.storage.put_scalars(timetest=12)

def get_freihand_dict(root_dir, split='training'):
    dataset_dicts = []

    uv_path = os.path.join(root_dir, "training_uv.npy")
    with open(uv_path, 'rb') as fid:
        uv = np.load(fid)

    images = []
    for root, dirs, files in os.walk(os.path.join(root_dir, split, 'rgb')):
        for idx, filename in enumerate(files):
            images.append(os.path.join(root, filename))
    images.sort()

    for idx, filename in enumerate(images):
        record = {}

        file_id = filename[-12:-4]

        if file_id != '00000000':
            json_index = int(file_id.lstrip("0")) % 32560
        else:
            json_index = 0

        gt_keypoints = uv[json_index, :, :]

        height, width = cv2.imread(filename).shape[:2]

        mask_filename = os.path.join(root_dir, split, 'mask', f'{json_index:08d}.jpg')
        mask = cv2.imread(mask_filename)
        segmented_pixels = np.argwhere(mask[:, :, 0] == 255)
        record['file_name'] = filename
        record['image_id'] = idx
        record['height'] = height
        record['width'] = width
        objs = []

        visibility = np.ones((1, 21)) * 2
        gt_keypoints = np.vstack((gt_keypoints, visibility))
        gt_keypoints = gt_keypoints.T
        obj = {
            "bbox": [np.min(segmented_pixels[:, 1]), np.min(segmented_pixels[:, 0]),
                     np.max(segmented_pixels[:, 1]), np.max(segmented_pixels[:, 0])],
            "bbox_mode": BoxMode.XYXY_ABS,
            "category_id": 0,
            "keypoints": gt_keypoints.tolist(),
            "num_keypoints": 21
        }

        record["annotation"] = [obj]

        dataset_dicts.append(record)

    return dataset_dicts

class Trainer(DefaultTrainer):

    def __init__(self,cfg):
        super().__init__(cfg)

    @classmethod
    def build_evaluator(cls, cfg, dataset_name, output_folder=None):
        if output_folder is None:
            output_folder = os.path.join(cfg.OUTPUT_DIR, "inference")
        return COCOEvaluator(dataset_name, cfg, True, output_folder)

    def build_hooks(self):

        def mapper(dataset_dict):
            dataset_dict = copy.deepcopy(dataset_dict)

            image = utils.read_image(dataset_dict["file_name"], format="BGR")
            image = torch.from_numpy(image.copy()).permute(2,0,1)

            return {
                "image": image,
                "height": dataset_dict['height'],
                "width": dataset_dict['width'],
                "instances": utils.annotations_to_instances(dataset_dict['annotation'],
                                                            (dataset_dict['height'], dataset_dict['width']))
            }
        hooks = super().build_hooks()
        hooks.insert(-1, LossEvalHook(
            13024,
            self.model,
            build_detection_test_loader(
                self.cfg,
                self.cfg.DATASETS.TEST[0],
                mapper=mapper
            )
        ))
        return hooks

    @classmethod
    def build_train_loader(cls, cfg):
        """
        Returns:
            iterable

        It now calls :func:`detectron2.data.build_detection_train_loader`.
        Overwrite it if you'd like a different data loader.
        """

        def mapper(dataset_dict):
            dataset_dict = copy.deepcopy(dataset_dict)

            image = utils.read_image(dataset_dict["file_name"], format="BGR")
            image = torch.from_numpy(image.copy()).permute(2,0,1)

            return {
                "image": image,
                "height": dataset_dict['height'],
                "width": dataset_dict['width'],
                "instances": utils.annotations_to_instances(dataset_dict['annotation'],
                                                            (dataset_dict['height'], dataset_dict['width']))
            }

        return build_detection_train_loader(cfg, mapper=mapper)

d = 'train'

for d in ['training', 'validation']:
    DatasetCatalog.register(f"freihand_{d}", lambda d=d: get_freihand_dict(_ROOT_DIR, d))
    MetadataCatalog.get(f"freihand_{d}").keypoint_names = _KEYPOINT_NAMES
    MetadataCatalog.get(f"freihand_{d}").keypoint_flip_map = []
    MetadataCatalog.get(f"freihand_{d}").thing_classes = 'None'


parser = argparse.ArgumentParser()
parser.add_argument('--weights', type=str)

args = parser.parse_args()

def main(args):
    cfg = get_cfg()
    cfg.merge_from_file(model_zoo.get_config_file("COCO-Keypoints/keypoint_rcnn_R_50_FPN_3x.yaml"))
    cfg.DATASETS.TRAIN = ("freihand_training",)
    cfg.DATASETS.TEST = ("freihand_validation",)
    cfg.TEST.EVAL_PERIOD = 13024
    cfg.DATALOADER.NUM_WORKERS = 2
    cfg.SOLVER.IMS_PER_BATCH = 48
    cfg.SOLVER.BASE_LR = 0.00025  # pick a good LR
    cfg.SOLVER.MAX_ITER = 13024*10 #15000    # 300 iterations seems good enough for this toy dataset; you may need to train longer for a practical dataset
    cfg.MODEL.ROI_KEYPOINT_HEAD.NUM_KEYPOINTS = 21
    cfg.TEST.KEYPOINT_OKS_SIGMA = 1
    cfg.MODEL.KEYPOINT_ON = True
    cfg.OUTPUT_DIR = "./logs/train_data/" + dt.now().strftime("%Y%m%d-%H%M%S")

    if args.weights is None:
        logger.info('Using model zoo weights.')
        cfg.MODEL.WEIGHTS = model_zoo.get_checkpoint_url("COCO-Keypoints/keypoint_rcnn_R_50_FPN_3x.yaml")  # Let training initialize from model zoo
        resume = False
    else:
        logger.info(f'Using partial trained weights from {args.weights}')
        cfg.MODEL.WEIGHTS = args.weights
        print(cfg.MODEL.WEIGHTS)
        logger.info(cfg.MODEL.WEIGHTS)
        resume = True

    os.makedirs(cfg.OUTPUT_DIR, exist_ok=True)
    trainer = Trainer(cfg)
    trainer.resume_or_load(resume=resume)
    return trainer.train()

if __name__ == '__main__':
    launch(main, num_gpus_per_machine=3, num_machines=1,dist_url='auto',args=(args,))